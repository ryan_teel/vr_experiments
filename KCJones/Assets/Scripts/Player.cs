﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Events;

[System.Serializable] public class ToggleEvent: UnityEvent<bool>
{

}

public class Player : NetworkBehaviour {

    [SerializeField] ToggleEvent OnToggleShared;
    [SerializeField] ToggleEvent OnToggleLocal;
    [SerializeField] ToggleEvent OnToggleRemote;


    // Use this for initialization
    void Start()
    {
        EnablePlayer();
    }

    // Use this for initialization
	
	// Update is called once per frame
	void Update () {
		
	}

    void DisablePlayer()
    {
        OnToggleShared.Invoke(false);

        if (isLocalPlayer)
        {
            OnToggleLocal.Invoke(false);
        }
        else
        {
            OnToggleRemote.Invoke(false);
        }
    }

    void EnablePlayer()
    {
        OnToggleShared.Invoke(true);

        if (isLocalPlayer)
        {
            OnToggleLocal.Invoke(true);
        }
        else
        {
            OnToggleRemote.Invoke(true);
        }
    }
}
