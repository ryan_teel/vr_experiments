﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class laser : NetworkBehaviour {

    //public int player = 1;
    public ParticleSystem laserline;
    private bool canShoot;

    // Use this for initialization
	void Start () {
        //laserline = GetComponent<ParticleSystem>();
        //RpcEndFire();
        if (isLocalPlayer) { canShoot = true; }
        else { canShoot = false; }
	}
	
	// Update is called once per frame
	void Update () {
        if (!canShoot) { return; }
        int player = playerControllerId + 1;
        if (player == 1)
        {
            if (Input.GetButton("P1-Fire2"))
            {
                CmdStartFire();
            }
            else
            {
                CmdEndFire();
            }
        }
        else
        {
            CmdEndFire();
        }

    }


    [Command]
    void CmdStartFire()
    {
        RpcStartFire();
    }

    [Command]
    void CmdEndFire()
    {
        RpcEndFire();
    }

    [ClientRpc]
    void RpcStartFire()
    {
        var em = laserline.emission;
        em.enabled = true;
    }

    [ClientRpc]
    void RpcEndFire()
    {
        var em = laserline.emission;
        em.enabled = false;
    }

}
