﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pizza : MonoBehaviour {

    public Rigidbody2D rb2d;
    public Rigidbody2D item;
    public float speed_x = 1.0f;
    public float speed_y = 1.0f;
    public Transform hand;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void makePizza() {
        Rigidbody2D item_clone = (Rigidbody2D)Instantiate(item, hand.position, hand.rotation);
        float cur_speed_y = rb2d.velocity.y;
        float cur_speed_x = rb2d.velocity.x;
        item_clone.velocity = new Vector2(cur_speed_x + speed_x, speed_y);
        item_clone.AddTorque(75f);
    }
}
