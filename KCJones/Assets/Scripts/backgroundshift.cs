﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class backgroundshift : MonoBehaviour {

    public Transform playArea;
    public float offsetSize = 510f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(playArea.position.x - transform.position.x > 220)
        {
            transform.position = new Vector3(transform.position.x + offsetSize, transform.position.y, transform.position.z);
        }
	}
}
