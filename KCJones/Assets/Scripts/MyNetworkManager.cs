﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.Match;

public class MyNetworkManager : NetworkManager {


    private NetworkStartPosition[] spawnPoints;

    void Start()
    {
        spawnPoints = FindObjectsOfType<NetworkStartPosition>();

        StartMatchMaker();

        matchMaker.ListMatches(0, 10, "", true, 0, 0, OnMatchList);

    }

    public override void OnMatchList(bool success, string extendedInfo, List<MatchInfoSnapshot> matches)
    {
        if (success)
        {
            if (matches.Count == 0)
            {
                matchMaker.CreateMatch("default", 4, true, "", "", "", 0, 0, OnMatchCreate);
            }
            else {
                matchMaker.JoinMatch(matches[matches.Count - 1].networkId, "", "", "", 0, 0, OnMatchJoined);
            }
        }
    }

    // called when connected to a server
    public override void OnClientConnect(NetworkConnection conn)
    {
        ClientScene.Ready(conn);
        //add number of players depending on number of joystick connected
        string[] joys = Input.GetJoystickNames();

        int numPlayers = joys.Length;
        if(numPlayers == 0)
        {
            ClientScene.AddPlayer(0);
        }
        else
        {
            for(int i = 0; i< numPlayers; i++)
            {
                ClientScene.AddPlayer((short)i);
            }
        }
        
    }

    // called when a new player is added for a client
    public override void OnServerAddPlayer(NetworkConnection conn, short playerControllerId)
    {

        Vector3 playerSpawnPos = spawnPoints[0].transform.position;
        if(spawnPrefabs.Count > 0)
        {
            var fab = spawnPrefabs[0];
            spawnPrefabs.RemoveAt(0);
            var player = (GameObject)GameObject.Instantiate(fab, playerSpawnPos, Quaternion.identity);

            //player.GetComponent<Status>().playerID = (int)playerControllerId +1;

            NetworkServer.AddPlayerForConnection(conn, player, playerControllerId);
        }
    }
}
