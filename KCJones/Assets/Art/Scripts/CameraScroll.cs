﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScroll : MonoBehaviour {

    public float scrollSpeed = 1.0f;
    public float smoothing = 1f;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        Vector3 delta = new Vector3(transform.position.x + scrollSpeed, transform.position.y, transform.position.z);
        transform.position = Vector3.Lerp(transform.position, delta, smoothing * Time.deltaTime);
    }
}

