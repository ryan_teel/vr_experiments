﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Toss : NetworkBehaviour {

    public float speed_x = 35.0f;
    public float speed_y = 20.0f;
    private Rigidbody2D rb2d;
    private bool tossing;
    public NetworkAnimator anim;
    //public int player;

    // Use this for initialization
    void Start () {
        rb2d = GetComponent<Rigidbody2D>();
    }
	
	// Update is called once per frame
	void Update () {
        //currently can only have one live tossed item at a time.
        if (!isLocalPlayer)
        {
            return;
        }
        int player = playerControllerId + 1;
        if (player == 1)
        {
            if (Input.GetButtonDown("P1-Fire1") && !tossing)
            {

                CmdToss();
            }
        }
        else if(player == 2)
        {
            if (Input.GetButtonDown("P2-Fire1") && !tossing)
            {

                CmdToss();
            }
        }
        
    }

    [Command] void CmdToss()
    {
        RpcToss();
    }

    [ClientRpc] void RpcToss()
    {
        anim.animator.SetTrigger("toss");
    }
}
