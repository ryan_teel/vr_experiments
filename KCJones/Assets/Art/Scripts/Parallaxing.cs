﻿using UnityEngine;
using System.Collections;

public class Parallaxing : MonoBehaviour {

    public Transform[] bgs;
    private float[] scales;
    public float smoothing = 1f;

    public Transform cam;
    private Vector3 prevCamPos;

    void Awake()
    {
        //cam = Camera.main.transform;
    }
    
    // Use this for initialization
	void Start () {
        prevCamPos = cam.position;
        scales = new float[bgs.Length];
        for(int i= 0; i< bgs.Length; i++){
            scales[i] = bgs[i].position.z * -1;
        }

	}
	
	// Update is called once per frame
	void FixedUpdate () {
        for(int i=0; i< bgs.Length; i++)
        {
            float prlx = (prevCamPos.x - cam.position.x) * scales[i];
            float bgTargetX = bgs[i].position.x + prlx;
            Vector3 bgTarget = new Vector3(bgTargetX, bgs[i].position.y, bgs[i].position.z);
            bgs[i].position = Vector3.Lerp(bgs[i].position, bgTarget, smoothing * Time.deltaTime);
        }
        prevCamPos = cam.position;
    }
}
