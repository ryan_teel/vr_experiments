﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class SimpleController : NetworkBehaviour {
    [HideInInspector] public bool jump = false;
    public bool doubleJump = false;

    public float moveForce = 500f;
    public float maxSpeed = 25f;
    public float minSpeed = 2f;
    public float idleSpeed = 12f;
    public float jumpForce = 1600f;
    public int jumpCount;
    public Transform groundCheck;
    //public int player;

    public NetworkAnimator anim;

    private bool grounded = false;
    private Rigidbody2D rb2d;

	// Use this for initialization
	void Start () {
        rb2d = GetComponent<Rigidbody2D>();
        jump = false;
        jumpCount = 0;
	}
	
	// Update is called once per frame
	void Update () {

        if (!isLocalPlayer)
        {
            return;
        }
        int player = playerControllerId + 1;

        if (Input.GetButtonDown("Cancel"))
        {
            Application.Quit();

        }
        grounded = Physics2D.Linecast(transform.position,groundCheck.position, 1<< LayerMask.NameToLayer("Ground"));
        if (grounded)
        {
            jumpCount = 0;
        }
        if(player == 1)
        {
            if (Input.GetButtonDown("P1-Jump") && grounded)
            {
                jump = true;
                jumpCount = 1;

            }
            else if (Input.GetButtonDown("P1-Jump") && !grounded && jumpCount < 2)
            {
                jumpCount = 2;
                doubleJump = true;
                //anim.SetTrigger("flip");
                CmdFlip();
            }
        }
        else if(player == 2)
        {
            if (Input.GetButtonDown("P2-Jump") && grounded)
            {
                jump = true;
                jumpCount = 1;

            }
            else if (Input.GetButtonDown("P2-Jump") && !grounded && jumpCount < 2)
            {
                jumpCount = 2;
                doubleJump = true;
                //anim.SetTrigger("flip");
                CmdFlip();
            }
        }
	}
    
    void FixedUpdate()
    {
        if (!isLocalPlayer)
        {
            return;
        }
        int player = playerControllerId + 1;
        float h = 0;
        if(player == 1)
        {
            h = Input.GetAxis("P1-Horizontal");
        }
        else if (player == 2)
        {
            h = Input.GetAxis("P2-Horizontal");
        }
        

        if (h > 0 )
        {
            rb2d.velocity = new Vector2(maxSpeed, rb2d.velocity.y);
            //rb2d.velocity = Vector2.Lerp(rb2d.velocity,new Vector2(maxSpeed, rb2d.velocity.y),.75f);
        }
        else if (h < 0)
        {
            rb2d.velocity = new Vector2(minSpeed, rb2d.velocity.y);
        }
        else
        {
            rb2d.velocity = new Vector2(idleSpeed, rb2d.velocity.y);
        }
        
        if (jump)
        {
            //rb2d.velocity = new Vector2(rb2d.velocity.x, 2*maxSpeed);
            rb2d.AddForce(new Vector2(0f, jumpForce));
            jump = false;
        }
        if (doubleJump)
        {
            //rb2d.velocity = new Vector2(rb2d.velocity.x, 2*maxSpeed);
            rb2d.AddForce(new Vector2(0f, jumpForce));
            doubleJump = false;
        }
    }

    [Command]
    void CmdFlip()
    {
        RpcFlip();
    }

    [ClientRpc]
    void RpcFlip()
    {
        anim.animator.SetTrigger("flip");
    }

}
